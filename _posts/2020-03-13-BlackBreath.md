---
layout: page
title:  "Black_Breath."
date:   2020-03-13 12:00:00 +0900
categories:
---

A plague burns through the planet like <span style="color: red;">lost fire.</span>

There's a <span style="color: red;">nervous, uncertain static</span> in the air.

Like the atmosphere before a storm.

The smell of rain in the distance.

Just beyond the horizon, <span style="color: red;">The Four Thunders</span> bellow out.

Hair stands up as a brambly feeling rolls over the back your neck.

As if <span style="color: red;">The Pale, Icy Hand</span> places its arm around you; welcoming you like an old friend.

You look on in horror at the Pestilent Fields.

Elysium crumbles to <span style="color: red;">Putrefied Rot</span>.

There's a collective feeling in our species stomach right now.

Turbulence.

That moment of free fall.

The <span style="color: red;">sinking feeling</span> that nothing can ever be the same again.

For the first time - we feel ourselves hurtling through the infinity of the cosmos.

All of us mere <span style="color: red;">passengers. A bacteria, clinging helplessly to a moldy rock</span> as it spirals through the cold vacuum of infinity.

Callous. Uncaring.

A single celled organism <span style="color: red;">unravels our RNA</span>.

Hollywood has trained us <span style="color: red;">like lapdogs</span> to believe we're _worth_ saving.

This may not be the end of the world - but <span style="color: red;">you can see it from here</span>.

The River of Time has once again ferried a concussive reality to the banks of our fragile species.

Whoever named <span style="color: red;">"Mother Nature"</span> had a terrible mother.
