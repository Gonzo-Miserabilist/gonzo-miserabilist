---
layout: page
title:  "The Lovers."
date:   2020-01-09 12:00:00 +0900
categories:
---

-----

## > <span style="color: red;">Death, The Devil, and The Tower.</span>

-----

The parting words from my last relationship are seared into the acrid flesh of my rotten heart.

This Heart - <span style="color: red;">This Engine of Ruin</span> - circulates my corrosive, caustic poison through the channels of my life.

Her words - <span style="color: grey;">This Epitaph</span> - reached backwards through time and curdled everything.

Every soft whisper. Every incurable laugh.

Every night falling asleep to the little bellows of her breathing.

Every hour spent listening to the rain.

Every soft and sweet conversation about feeling whole.

All of it rendered opaque in an instant.

<span style="color: red;">"I find myself so much happier when you're not around."</span>

---

And just like that - we'd lost cabin pressure.

The all too familiar feeling that nothing would ever be the same.

Her words washed over me like a coroner's cloth.

And then it was over.

-----
## > <span style="color: red;">Judgement, The Moon, and The Hermit.</span>
-----

Armed with the realization of my own depth for cruelty; I ventured out into the underworld in my mind.

There was a homecoming in the hellscape of my brain.

While building my blood soaked altar - She convinced me not to cut what I couldn't untie.

Things were calm for a while, as we gingerly untangled our lives from one another.

However - if you've read more than a sentence here - you'll know that's not what happened.

I buried the pain of <span style="color: red;">my failed romance</span>.

I buried the pain of <span style="color: red;">her new romance</span>.

<span style="color: red;">I buried the pain of my dead.</span>

---

This is when something happened that I never planned for.

The moment I saw <span style="color: red;">[her]</span>, was the moment I understood my true capacity for <span style="color: red;">lonliness</span>.

I'd sought isolation my entire life.

When her eyes looked into me the first time; that's when I felt true sadness swaddling me like a child.

My mind became Dresden.

Sudden, Intrusive, and Unwavering thoughts fire bombed my life.

Loneliness like nothing I'd ever known had taken hold of me. I could count on nothing.

-----

## > <span style="color: red;"> The Chariot, The Magician, and The Hanged Man.

-----

How could my brain (it's creativity rivaled only by houseplants and dinner plates) dream such a deep and cosmic connection with someone?

Why, even now, after the lines in the ash have been drawn, and the scorched earth of my cerebral cataclysm laid bare...

Why do I still feel loss for someone who never even wasted a thought on me? Why do I feel the loss of my kindred spirit?

Maybe we shared a past life.

Maybe in another universe so close to ours we can feel each other through the ragged and tattered fabric of reality.

As if we're waiting for each other just on the other side of the cosmic hallway.

No.

<font color="red">The truth is that I was never worth thinking about.</font>

Like an animal sprang from a trap, I forced myself under her care.

I looked to her for <span style="color: red;">absolution</span>.

I sought her to absolve me of the <span style="color: red;">pain</span>, and the <span style="color: red;">guilt</span>, and the <span style="color: red;">embarrassment</span>, and the <span style="color: red;">self-loathing</span>, and <span style="color: red;">disgust of myself</span>.

Eventually - She learned to not feed a stray.

Eventually - I learned that nobody can save me from myself.

All she wants in return is a cold shoulder.

When I find myself in a hole - I stop digging.

Even as I imagine her siren's song beckoning me further down.

<font color="red">Dig. Dig. Dig.</font>

I still hear her like a leering poltergeist.

I still feel her like a phantom limb.

The chewed tissue exposed to the harsh winds.

I feel the <span style="color: red;">soured and sticky blood</span> drying into blackened mud.

The cauterized flesh wraps around the bone like a drawstring pouch.

I smell <span style="color: red;">Iron and Rot</span>.

But in spite of it all: I appreciate the fleeting anesthesia the day-dreams brought me.

But I suspect I've felt everything that I'll ever feel.

<span style="color: red;">I lost a kindred spirit.</span>

I lost a part my larger, cosmic self.

It's time to get moving.

Let Go. Move On.

You are strangers. You are not friends.

<font color="red">It was never real.</font>

You have to keep moving.

It doesn't matter how many skies have fallen.
