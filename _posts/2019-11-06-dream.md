---
layout: page
title:  "Dream."
date:   2019-11-06 01:11:00 +0100
categories:
---

We fall into each other the way one falls asleep: slowly, then all at once.

She's a <span style="color: red;">crimson woman</span> in my black and white world.

I want to explore the deep and expansive forests of her heart; where the trees are so strong, viking shipwrights would have thought they'd seen Yggdrasil itself. Full of life, and untouched by the atrocities of humanity.

I want to swim in the boundless oceans of her mind. It's playful subsurface fish, and it's murky frightening depths. Her sirens song beckoning me deeper. I have no fear of the deepening blackness. I have no fear of the uncharted twilight zones of her mind. The deeper I go, the higher the pressure. The higher the pressure, the more the water holds me.

Happiness is traversing every mountain, every hill, and every intricacy of her body. Tracing over every perfect flaw. My fingertips moving over her like a paintbrush. I'm a Spanish explorer on the coast of a new world.

We look into each others eyes. I run my fingers through her gilded curls, then along her ivory cheek. Her eyes are oceans in a storm. The deepest and blackest blue. <span style="color: red;">Powerful</span>. Frightening. They draw out honesty, often against my will. They reflect my true self. There's truth in beauty, but not the other way around. It's a reminder to stop being sorry, and to be better.

The space between our lips disappears. My hands on her hips pull her closer as she snakes around me.

Time bleeds. The universe is on a track to return itself to equilibrium. I feel us moving closer to that completion. We're tiny particles of a microbe in the infinity of the cosmos. But in this one moment, the universe collapses into a singularity. We're frozen in time as it bleeds into all possible fates. As the galaxies collapse we are pulled apart, together, into the life stream. We are shredded and wrapped into the folds of the stars. We are strewn through the event horizon.

Every synapse in my brain misfires. Cold electricity crawls through the subterranean caves in my skull. Anguish wanders through my bones like a <span style="color: red;">lost fire</span>.

I wake up to see her wither into ashes before my tired eyes.

My terminal blinks impatiently as it awaits input.

<font color="red">~:$_ </font>

Am I so different from this terminal? Impatiently blinking. Awaiting input.

I'm King Nebuchadnezzar. I have dreamed a dream. And now that dream is gone from me.
