---
layout: page
title:  "Briarheart"
date:   2022-07-12 00:00:01 +0600
categories:
---

# An empty heart, in an empty room, in an empty house.

---

It's been 10 years.

People say stupid shit like:

> "Wow! I can't believe it's been 10 years! It feels like yesterday!"

It's the quality of dialogue you'd expect from those basic cable lifetime movies.

The ones they make for bored house wives...

It's been 10 years. And I've felt every fucking second.

10 years. 100 years. A few hours. It's all the same to me now...

<font color="red">Each day is a false dawn.</font>

Nothing ever happens the way you think it will...

Like how I died _years_ ago, yet here I am drowning in the comfort of my suburban town house.

I guess you _can_ have your cake and eat it too.

It's been 10 years since the last time I believed someone loved me.

I've heard people say the words - but they feel weightless. Ephemeral. Ghosts of what once was - and will never be again.

The dying embers of a fire that was once my guiding light.

Out of all the voices in my head - hers was the only one that made sense.

Now those words are poison.

When I hear them - they decompose and ferment in my chest.

The seeds of Anguish germinate within me and bear their bitter fruit.

A crown of briars blankets my black heart.

<font color="red"> Those words were Hers.</font>

Now it feels like the people I love the most may just the people I hate the least.

And I hate every door she never walked through.

The dream is over - but The Night will never end.

