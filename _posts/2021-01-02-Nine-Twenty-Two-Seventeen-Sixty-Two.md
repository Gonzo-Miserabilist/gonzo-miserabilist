---
layout: page
title:  "Nine Twenty-Two Seventeen Sixty-Two Part_01"
date:   2021-01-02 00:00:01 +0600
categories:
---

# New_Year - New `Kernel_Panic`

-----

The elevator doors opened to a bright, trendy office roused with _peppy_ young professionals.

The sounds of typing, clicking, and warm hearted client calls composed a symphony titled:

> "_Productivity Theater._"

Everyone was bright-eyed and bushy tailed; dozens of puckish young go-getters.

I felt like a whore in church.

While scanning the horizon for _any_ exit; I noticed a determined Young Woman scowling at her computer. She crinkled her nose at some office banality, removed her headphones, and pushed herself away from her desk.

She picked up her coffee mug, and set out on a quest for stimulants.

Without warning, I watched her pivot on her heels, deviating from her path and reroute towards me.

I wondered if this is how rats feel as they consign their life to a hawk.

<font color="red">I could feel myself being rendered opaque.</font>

It was as if we had always met there, in that exact spot, in this exact moment, a hundred lifetimes in a row.

<font color="red">She moved like long hair in slow motion.</font>

In one graceful gesture, she arrived at a _bouncy little halt_ and stuck out her hand to introduce herself.

Her slender fingers wrapped around my hand like dainty pythons.

We exchanged one of those firm, professional hand shakes that old white men don't _shut the fuck up about._

Her smile lingered below those endearing, dusky, dark brown eyes like a Cheshire Cat.

Her name was the sound of sirens to the inferno in my mind.

We determined our paths were yoked:

_She wished to be caffeinated. I wished to be employed._

We set off across the office in a haiku version of the hero's journey.

Like a Valkyrie, she delievered me to an Elysian Field of cheap colorful cushioned couches.

She smiled again, wished me luck, and sauntered off to conquer some other world.

For a moment, I imagined we were friends; but I would have settled for being memories.

> <font color="red">sudo kill $(pidof blacksheep.py)</font>

