I remember this feeling.

It was nearly `04:00` AM on a fall morning - and I was in the grips of a void like I'd never known.

For hours and hours I watch a blinking `I-Beam` beckoning me for input.

Some essay about something. Not even worth remembering.

I do remember the essay just before that one. I'll never forget it.

Shirley Jackson's `The Lottery.` One of my favorites - so the essay was cinch.

My lifelong data archival practices pulled through, allowing me to compile a new essay based out of old notes.

I finished early - and met my friend Brandon at our usual spot; a local hibachi grill.

Our years of patronage put us in the _excelsior-VIP-members-only-gold-tier_ of strip-mall hibachi eateries.

Thanks to the waitress & line cook's employee discounts getting our tickets down to the child price - Brandon and I concluded our `$7.00` lunch buffet ritual - and parted ways until the next tuesday.

I remember it - because it was the last time I'd see Katsumi, Solomon, or Brandon.

I'd unknowingly committed the most _vile_ of academic crimes in the eyes of my "Professor"* - and there was but one recourse -- A first-strike on my academic record for _plagiarizing._ (He boasted this was one of his favorite tricks.)

The source of my _original sin_? I didn't cite a source when talking about _Mr. Graves_ but I DID cite a source when referring to Mrs. Delacroix. (Referring to the foreshadowing in their names.)

You see - _Delacroix_ is actually Latin for _community college professor power trip_. [* citation needed.]

So - when the time came for my next essay - I couldn't shake the feeling that nothing from that moment forward would ever be the same.

If I didn't turn in _anything_ I'd be kicked out of my esteemed academic institute of community education.

If I turned in something with a mistake - maybe it would be fine. _or_ it would leave a permanent warning on my academic record - instead of the 1-year scarlet letter I found myself with.

And then -- just like now -- I just can't shake the feeling that _from this moment forward_ -- nothing will ever be the same again.

It seems the machinations of my rusty people skills, have led me to burn through the hand I was dealt.

And it's not really worth the emotional bandwidth to reflect back, longing for opportunities that might have put me in a more comfortable position.

Fools trip on what's behind them. No use wondering about hypotheticals - the world itself is vexing enough.

* This is in quotes because he hated the term professor - and would try to steer us towards managerial terms. He cited his extensive career in various fast-food management roles for this? I'm not knockin' the work, the point is this guy _loved_ being a _manager._ Real straight shooter.
